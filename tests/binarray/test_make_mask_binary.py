import binarray
import cocotb

def test_make_zero_mask_binary_little_hi_border():
    """Тест для функции создания масок BinaryValue
    """
    mask_len = 32
    hi_idx = 31
    lo_idx = 24
    big_endian = False
    zero_mask = True
    ref_val = "00000000111111111111111111111111"

    ret = binarray.make_mask_binary_val(
        mask_len=mask_len,
        hi_idx=hi_idx,
        lo_idx=lo_idx,
        big_endian=big_endian,
        zero_mask=zero_mask
        )
    assert ret == ref_val

def test_make_zero_mask_binary_big_hi_border():
    mask_len = 32
    hi_idx = 31
    lo_idx = 24
    big_endian = True
    zero_mask = True
    ref_val = "11111111111111111111111100000000"

    ret = binarray.make_mask_binary_val(
        mask_len=mask_len,
        hi_idx=hi_idx,
        lo_idx=lo_idx,
        big_endian=big_endian,
        zero_mask=zero_mask
        )
    assert ret == ref_val

def test_make_zero_mask_binary_little_range():
    mask_len = 32
    hi_idx = 17
    lo_idx = 12
    big_endian = False
    zero_mask = True
    ref_val = "11111111111111000000111111111111"

    ret = binarray.make_mask_binary_val(
        mask_len=mask_len,
        hi_idx=hi_idx,
        lo_idx=lo_idx,
        big_endian=big_endian,
        zero_mask=zero_mask
        )
    assert ret == ref_val

def test_make_zero_mask_binary_big_range():
    mask_len = 32
    hi_idx = 17
    lo_idx = 12
    big_endian = True
    zero_mask = True
    ref_val = "11111111111100000011111111111111"

    ret = binarray.make_mask_binary_val(
        mask_len=mask_len,
        hi_idx=hi_idx,
        lo_idx=lo_idx,
        big_endian=big_endian,
        zero_mask=zero_mask
        )
    assert ret == ref_val

def test_make_zero_mask_binary_little_lo_border():
    mask_len = 32
    hi_idx = 7
    lo_idx = 0
    big_endian = False
    zero_mask = True
    ref_val = "11111111111111111111111100000000"

    ret = binarray.make_mask_binary_val(
        mask_len=mask_len,
        hi_idx=hi_idx,
        lo_idx=lo_idx,
        big_endian=big_endian,
        zero_mask=zero_mask
        )
    assert ret == ref_val

def test_make_zero_mask_binary_big_lo_border():
    mask_len = 32
    hi_idx = 7
    lo_idx = 0
    big_endian = True
    zero_mask = True
    ref_val = "00000000111111111111111111111111"

    ret = binarray.make_mask_binary_val(
        mask_len=mask_len,
        hi_idx=hi_idx,
        lo_idx=lo_idx,
        big_endian=big_endian,
        zero_mask=zero_mask
        )
    assert ret == ref_val

def test_make_ones_mask_binary_big_lo_border():
    mask_len = 32
    hi_idx = 7
    lo_idx = 0
    big_endian = True
    zero_mask = False
    ref_val = "11111111000000000000000000000000"

    ret = binarray.make_mask_binary_val(
        mask_len=mask_len,
        hi_idx=hi_idx,
        lo_idx=lo_idx,
        big_endian=big_endian,
        zero_mask=zero_mask
        )
    assert ret == ref_val

def test_make_ones_mask_binary_big_lo_border_size():
    mask_len = 32
    size = 8
    lo_idx = 0
    big_endian = True
    zero_mask = False
    ref_val = "11111111000000000000000000000000"

    ret = binarray.make_mask_binary_val_size(
        mask_len=mask_len,
        size=size,
        lo_idx=lo_idx,
        big_endian=big_endian,
        zero_mask=zero_mask
        )
    assert ret == ref_val

def test_make_zero_mask_binary_little_hi_border():
    mask_len = 32
    size = 8
    lo_idx = 24
    big_endian = False
    zero_mask = True
    ref_val = "00000000111111111111111111111111"

    ret = binarray.make_mask_binary_val_size(
        mask_len=mask_len,
        size=size,
        lo_idx=lo_idx,
        big_endian=big_endian,
        zero_mask=zero_mask
        )
    assert ret == ref_val

def test_make_zero_mask_binary_little_hi_border():
    mask_len = 64
    size = 16
    lo_idx = 24
    big_endian = False
    zero_mask = True
    ref_val = "1111111111111111111111110000000000000000111111111111111111111111"

    ret = binarray.make_mask_binary_val_size(
        mask_len=mask_len,
        size=size,
        lo_idx=lo_idx,
        big_endian=big_endian,
        zero_mask=zero_mask
        )
    assert ret == ref_val

