import binarray
import cocotb

def test_get_segment_from_vector_0():

    init_value = "00010001001000100011001101000100010101010110011001110111100010001001100101010110"
    nbits = 80
    lo_idx = 30
    big_endian = False
    left2ridght = True
    set_val = 0xb  #"00000000000001011"
    set_val_bits = 17
    ref_val = "00010001001000100011001101000100000000000001011001110111100010001001100101010110"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    set_val = cocotb.binary.BinaryValue(value=set_val, n_bits=set_val_bits,
                                        bigEndian=big_endian)

    ret = binarray.set_segment_in_vector(vector_handle=bin_val,
                                         sector_handle=set_val, lo_idx=lo_idx,
                                         size=set_val_bits,
                                         big_endian=big_endian,
                                         left2ridght=left2ridght)
    assert ret.binstr == ref_val


def test_get_segment_from_vector_1():

    init_value = "00010001001000100011001101000100010101010110011001110111100010001001100101010110"
    nbits = 80
    lo_idx = 33
    big_endian = False
    left2ridght = False
    set_val = 0xb  #"00000000000001011"
    set_val_bits = 17
    ref_val = "00010001001000100011001101000100000000000001011001110111100010001001100101010110"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    set_val = cocotb.binary.BinaryValue(value=set_val, n_bits=set_val_bits,
                                        bigEndian=big_endian)

    ret = binarray.set_segment_in_vector(vector_handle=bin_val,
                                         sector_handle=set_val, lo_idx=lo_idx,
                                         size=set_val_bits,
                                         big_endian=big_endian,
                                         left2ridght=left2ridght)
    assert ret.binstr == ref_val


def test_get_segment_from_vector_2():

    init_value = "00010001001000100011001101000100010101010110011001110111100010001001100101010110"
    nbits = 80
    lo_idx = 3
    big_endian = False
    left2ridght = True
    set_val = 0xab  #"00000000010101011"
    set_val_bits = 17
    ref_val = "00000000000010101011001101000100010101010110011001110111100010001001100101010110"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    set_val = cocotb.binary.BinaryValue(value=set_val, n_bits=set_val_bits,
                                        bigEndian=big_endian)

    ret = binarray.set_segment_in_vector(vector_handle=bin_val,
                                         sector_handle=set_val, lo_idx=lo_idx,
                                         size=set_val_bits,
                                         big_endian=big_endian,
                                         left2ridght=left2ridght)
    assert ret.binstr == ref_val


def test_get_segment_from_vector_3():

    init_value = "00010001001000100011001101000100010101010110011001110111100010001001100101010110"
    nbits = 80
    lo_idx = 3
    big_endian = False
    left2ridght = False
    set_val = 0xab  #"00000000010101011"
    set_val_bits = 17
    ref_val = "00010001001000100011001101000100010101010110011001110111100000000000010101011110"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    set_val = cocotb.binary.BinaryValue(value=set_val, n_bits=set_val_bits,
                                        bigEndian=big_endian)

    ret = binarray.set_segment_in_vector(vector_handle=bin_val,
                                         sector_handle=set_val, lo_idx=lo_idx,
                                         size=set_val_bits,
                                         big_endian=big_endian,
                                         left2ridght=left2ridght)
    assert ret.binstr == ref_val


def test_get_segment_from_vector_4():

    init_value = "00010001001000100011001101000100010101010110011001110111100010001001100101010110"
    nbits = 80
    lo_idx = 0
    big_endian = False
    left2ridght = True
    set_val = 0x1e000  #"11110000000000000"
    set_val_bits = 17
    ref_val = "11110000000000000011001101000100010101010110011001110111100010001001100101010110"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    set_val = cocotb.binary.BinaryValue(value=set_val, n_bits=set_val_bits,
                                        bigEndian=big_endian)

    ret = binarray.set_segment_in_vector(vector_handle=bin_val,
                                         sector_handle=set_val, lo_idx=lo_idx,
                                         size=set_val_bits,
                                         big_endian=big_endian,
                                         left2ridght=left2ridght)
    assert ret.binstr == ref_val


def test_get_segment_from_vector_5():

    init_value = "00010001001000100011001101000100010101010110011001110111100010001001100101010110"
    nbits = 80
    lo_idx = 0
    big_endian = True
    left2ridght = False
    set_val = 0x1e000  #"11110000000000000"
    set_val_bits = 17
    ref_val = "00010001001000100011001101000100010101010110011001110111100010011110000000000000"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    set_val = cocotb.binary.BinaryValue(value=set_val, n_bits=set_val_bits,
                                        bigEndian=big_endian)

    ret = binarray.set_segment_in_vector(vector_handle=bin_val,
                                         sector_handle=set_val, lo_idx=lo_idx,
                                         size=set_val_bits,
                                         big_endian=big_endian,
                                         left2ridght=left2ridght)
    assert ret.binstr == ref_val

