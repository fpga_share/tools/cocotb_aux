import binarray
import cocotb

def test_get_segment_from_vector_0():

    init_value = 0x11223344556677889956
    nbits = 80
    lo_idx = 0
    hi_idx = 7
    big_endian = False
    left2ridght = True
    ref_val = "00010001"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    ret = binarray.get_segment_from_vector(bin_val, lo_idx, hi_idx,
                                           big_endian=big_endian,
                                           left2ridght=left2ridght)
    assert ret.binstr == ref_val

def test_get_segment_from_vector_1():

    init_value = 0x11223344556677889956
    nbits = 80
    lo_idx = 0
    hi_idx = 7
    big_endian = True
    left2ridght = True
    ref_val = "10001001"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    ret = binarray.get_segment_from_vector(bin_val, lo_idx, hi_idx,
                                           big_endian=big_endian,
                                           left2ridght=left2ridght)
    assert ret.binstr == ref_val

def test_get_segment_from_vector_2():

    init_value = 0x11223344556677889956
    nbits = 80
    lo_idx = 0
    hi_idx = 7
    big_endian = False
    left2ridght = False
    ref_val = "01010110"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    ret = binarray.get_segment_from_vector(bin_val, lo_idx, hi_idx,
                                           big_endian=big_endian,
                                           left2ridght=left2ridght)
    assert ret.binstr == ref_val

def test_get_segment_from_vector_3():

    init_value = "00010001001000100011001101000100010101010110011001110111100010001001100101010110"
    nbits = 80
    lo_idx = 0
    hi_idx = 7
    big_endian = True
    left2ridght = True
    ref_val = "00010001"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    ret = binarray.get_segment_from_vector(bin_val, lo_idx, hi_idx,
                                           big_endian=big_endian,
                                           left2ridght=left2ridght)
    assert ret.binstr == ref_val

def test_get_segment_from_vector_4():

    init_value = "00010001001000100011001101000100010101010110011001110111100010001001100101010110"
    nbits = 80
    lo_idx = 0
    hi_idx = 7
    big_endian = False
    left2ridght = False
    ref_val = "01010110"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    ret = binarray.get_segment_from_vector(bin_val, lo_idx, hi_idx,
                                           big_endian=big_endian,
                                           left2ridght=left2ridght)
    assert ret.binstr == ref_val

def test_get_segment_from_vector_5():

    init_value = "00000000000000000000000000000000110111111110000000000000000000000000000000000000"
    nbits = 80
    lo_idx = 32
    hi_idx = 42
    big_endian = False
    left2ridght = True
    ref_val = "11011111111"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    ret = binarray.get_segment_from_vector(bin_val, lo_idx, hi_idx,
                                           big_endian=big_endian,
                                           left2ridght=left2ridght)
    assert ret.binstr == ref_val

def test_get_segment_from_vector_6():

    init_value = "00000000000000000000000000000000110111111110000000000000000000000000000000000000"
    nbits = 80
    lo_idx = 37
    hi_idx = 47
    big_endian = False
    left2ridght = False
    ref_val = "11011111111"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    ret = binarray.get_segment_from_vector(bin_val, lo_idx, hi_idx,
                                           big_endian=big_endian,
                                           left2ridght=left2ridght)
    assert ret.binstr == ref_val

def test_get_segment_from_vector_7():

    init_value = "00000000000000000000000000000000110111111110000000000000000000000000000000000000"
    nbits = 80
    lo_idx = 37
    hi_idx = 47
    big_endian = True
    left2ridght = False
    ref_val = "11011111111"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    ret = binarray.get_segment_from_vector(bin_val, lo_idx, hi_idx,
                                           big_endian=big_endian,
                                           left2ridght=left2ridght)
    assert ret.binstr == ref_val

def test_get_segment_from_vector_8():

    init_value = "00000000000000000000000000000000110111111110000000000000000000000000000000000000"
    nbits = 80
    lo_idx = 37
    size = 11
    big_endian = True
    left2ridght = False
    ref_val = "11011111111"

    bin_val = cocotb.binary.BinaryValue(value=init_value, n_bits=nbits,
                                        bigEndian=big_endian)

    ret = binarray.get_segment_from_vector_size(vector_handle=bin_val,
                                                lo_idx=lo_idx, 
                                                size=size,
                                                big_endian=big_endian,
                                                left2ridght=left2ridght
                                                )
    assert ret.binstr == ref_val
