"""Модуль с вспомогательными функциями для работ с битовыми векторами в
   cocotb.binary
"""
from cocotb.binary import BinaryValue


def make_mask_binary_val(mask_len: int, hi_idx: int, lo_idx: int,
                         big_endian=False, zero_mask=True) -> str:
    """Сформировать битовую маску вида 11100011, или 00011100

       Функция вернут строку указанной длины состоящую из 0/1, в которой все
       биты (в строковом представлении) будут равны "1" или "0" (в зависимости
       от флага zero_mask) за исключением сегмента заключенного между
       [hi_idx:lo_idx], который будет иметь противоположное значение.

       Функция предназначена преимуществено для работы с
       cocotb.binary.BinaryValue, который может принимать строковое значение
       бинарной строки произвольной длины (большей, чем разрядность ЭВМ),
       поэтому вместо битовых преобразований используются строковые.

       Параметры:

            mask_len: длина маски (в битах);

            hi_idx: старший индекс сегмента;

            lo_idx: младший индекс сегмента;

            big_endian: является ли вектор bigEndian;

            zero_mask: должен ли указанный сегмент в маске быть заполнен
                       нулями, или единицами

       Возвращаемое значение:

            Сформированная битовая маска в строковом представлении
    """
    if hi_idx > mask_len:
        raise ValueError(f"hi_idx больше чем размер вектора: {hi_idx=}")
    if lo_idx < 0:
        raise ValueError(f"lo_idx меньше нуля: {lo_idx=}")

    if zero_mask:
        targ_symb = "0"
        non_targ_symb = "1"
    else:
        targ_symb = "1"
        non_targ_symb = "0"

    if big_endian:
        right_idx = hi_idx
        left_idx = lo_idx
    else:
        right_idx = mask_len - 1 - lo_idx
        left_idx = mask_len - 1 - hi_idx

    mask_val = ""
    for i in range(mask_len):
        if i < left_idx:
            mask_val += non_targ_symb
            continue
        if (i >= left_idx) & (i <= right_idx):
            mask_val += targ_symb
            continue
        mask_val += non_targ_symb
    return mask_val


def make_mask_binary_val_size(mask_len: int, size: int, lo_idx: int,
                              big_endian=False, zero_mask=True) -> str:
    """То же, что и make_mask_binary_val, но вместо пары индексов задается
       нижний индекс, и размер формируемого поля

       Параметры:

            mask_len: длина маски (в битах);

            size: размер устанавливаемого сегмента:;

            lo_idx: младший индекс сегмента;

            big_endian: является ли вектор bigEndian;

            zero_mask: должен ли указанный сегмент в маске быть заполнен
                       нулями, или единицами

       Возвращаемое значение:

            Сформированная битовая маска в строковом представлении
    """
    return make_mask_binary_val(mask_len, lo_idx + size - 1, lo_idx,
                                big_endian, zero_mask)


def get_segment_from_vector(vector_handle: BinaryValue, lo_idx: int, hi_idx:
                            int, big_endian=False,
                            left2ridght=False) -> BinaryValue:
    """Вернуть сегмент вектора

       Функция принимает на вход логический вектор в формате BinaryValue, и
       индексы сегмента этого вектора, который нужно вернуть.

       На выходе будет возвращаен объект BinaryValue равный указанному сегменту
       от изначального вектора

       Параметры:

            vector_handle: исходный вектор, часть которого нужно вернуть
            lo_idx: нижний индекс сегмента;
            hi_idx: верхний индекс сегмента;
            big_endian: признак типа big/little endian;
            left2ridght: Слева на право считывать вектор, или наоборот

       Возвращаемое значение:

            Сформированный BinaryValue объект хранящий указанный сегмент

    """

    n_bits: int = vector_handle.n_bits                          # type: ignore

    if hi_idx > n_bits:
        raise ValueError(
            f"hi_idx больше чем размер вектора: {n_bits=}"
            )
    if lo_idx < 0:
        raise ValueError(f"lo_idx меньше нуля: {lo_idx=}")
    if lo_idx > hi_idx:
        raise ValueError(f"lo_idx ({lo_idx=}) больше чем hi_idx ({hi_idx=})")

    if left2ridght:
        right_idx = hi_idx
        left_idx = lo_idx
    else:
        right_idx = n_bits - 1 - lo_idx
        left_idx = n_bits - 1 - hi_idx

    out_val_range = right_idx - left_idx + 1
    ret_binstr = [''] * out_val_range
    for i in range(out_val_range):
        in_idx = left_idx + i
        ret_binstr[i] = vector_handle.binstr[in_idx]

    ret_binstr = ''.join(ret_binstr)

    return BinaryValue(value=ret_binstr, n_bits=out_val_range,
                       bigEndian=big_endian)


def get_segment_from_vector_size(vector_handle: BinaryValue, lo_idx: int, size:
                                 int, big_endian=False,
                                 left2ridght=False) -> BinaryValue:
    """Вернуть сегмент вектора

       То же самое, что и get_segment_from_vector, но принимает аргументом не
       индексы сегмента, а его начальное положение и размер.


       Параметры:

            vector_handle: исходный вектор, часть которого нужно вернуть
            lo_idx: нижний индекс сегмента;
            size: размер сегмента (бит);
            big_endian: признак типа big/little endian;
            left2ridght: Слева на право считывать вектор, или наоборот

       Возвращаемое значение:

            Сформированный BinaryValue объект хранящий указанный сегмент
    """
    return get_segment_from_vector(vector_handle=vector_handle,
                                   lo_idx=lo_idx,
                                   hi_idx=lo_idx + size - 1,
                                   big_endian=big_endian,
                                   left2ridght=left2ridght
                                   )


def set_segment_in_vector(vector_handle: BinaryValue,  # pylint: disable=R0913
                          sector_handle: BinaryValue,
                          lo_idx: int,
                          size: int,
                          big_endian=False,
                          left2ridght=False
                          ) -> BinaryValue:
    """Установить значение регистра порта из общего массива

    """
    sector_width: int = sector_handle.n_bits                    # type: ignore
    vector_width: int = vector_handle.n_bits                    # type: ignore
    if sector_width > size:
        raise ValueError(
            f"Размер устанавливаемого значения {sector_width=} больше "
            f"размера выделенного сегмента в {size} бит"
            )
    hi_idx = size + lo_idx
    if hi_idx > vector_width:
        raise ValueError(
            f"Присваиваемый сегмент выходит {hi_idx} за границу исходного "
            f"вектора ({sector_width=} бит)"
            )

    tmp = BinaryValue(value=sector_handle.binstr, n_bits=size,
                      bigEndian=big_endian)

    if left2ridght:
        right_idx = hi_idx
        left_idx = lo_idx
    else:
        right_idx = vector_width - lo_idx
        left_idx = vector_width - hi_idx

    idx = -1
    res = ""
    for i in range(left_idx):
        idx += 1
        res += vector_handle.binstr[i]

    for i in range(size):
        idx += 1
        res += tmp.binstr[i]

    for i in range(right_idx, vector_width):
        res += vector_handle.binstr[i]

    return BinaryValue(value=res, n_bits=vector_width, bigEndian=big_endian)
